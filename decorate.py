import math
import time
from functools import lru_cache

def timer(func):
    def inner(x):
        starttime = time.time()
        func(x)
        endtime = time.time()
        return starttime-endtime
    return inner    

@timer
def sqrt(x):
    return math.sqrt(x)
@timer
def square(x):
    return x * x


def htimer(func):
    def inner(*iterables, **kiterables):
        starttime = time.time()
        func(*iterables, **kiterables)
        endtime = time.time()
        return starttime-endtime
    return inner 

@htimer
def add(x, y):
    return x + y

@htimer
def add3(x, y, z):
    return x + y + z

@htimer
def add_any(*args):
    return sum(args)

@htimer
def abs_add_any(*args, **kwargs):
    total = sum(args)
    if kwargs.get('abs') is True:
        return abs(total)
    return total

x = add_any(10, 20)
y = abs_add_any(1, 2, 3, 4, 5, 6, abs='True')
#print(x) 
#print(y)


def memoize(function):
    memory = {}

    def inner(arg):
        start = time.time()
        if memory.get(arg):
            end = time.time()
            print(str(end-start))
            return memory[arg]
        result = function(arg)
        memory[arg] = result
        end = time.time()
        print(end-start)
        return result
    return inner

@memoize
def fib(n):
    #print("Computing fib({})".format(n))
    #USING DYNAMIC PROGRAMMING TO SAVE RUNTIME
    lt = []
    lt.append(0)
    lt.append(1)
    for i in range(2, n+1):
        a = lt[i-1]
        b = lt[i-2]
        lt.append(a+b)

    return lt[n]     

    #if n in [0, 1]:
     #   return n
    #return fib(n - 1) + fib(n - 2)
    
    
print(fib(10))
print(fib(10))

#LRUCACHE
@lru_cache(maxsize=10)
def factorial(n):
    print("Computing factorial({})".format(n))
    fact = 1
    for i in range(1, n + 1):
        fact *= i
    return fact


print(factorial(5))
print(factorial(5))
print(factorial(10))


